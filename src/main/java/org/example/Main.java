package org.example;

import java.sql.Connection;
import java.util.Optional;

import org.example.dao.UserDao;
import org.example.models.User;

public class Main {
	public static void main(String[] args) {
		//TUTAJ ZAIMPLEMENTUJ GUI
		final ConnectionManager connectionManager = new ConnectionManager();
		final Connection connection = connectionManager.getConnection();
		final UserDao userDao = new UserDao(connection);
		userDao.createTableIfNotExists();

		//wypelnienie danymi
		userDao.save(new User("Adam", "admin123"));
		userDao.save(new User("Robot", "robot123"));

		//wypisanie wszystkich userow
		userDao.getAll().forEach(user -> System.out.println(user));
		//wypisywanie pojednyczego usera
//		userDao.get(1).ifPresent(user -> System.out.println(user));

		//usuwanie
		System.out.println("USUWANIE");
		userDao.remove(1);
		userDao.getAll().forEach(user -> System.out.println(user));

		//update
		final Optional<User> user = userDao.get(0);
		final User oldUser = user.get();
		oldUser.setUsername("UPDATED ADAM");
		userDao.update(oldUser);

		userDao.getAll().forEach(System.out::println);


		//transakcje
//		userDao.save(new User(98,"notUpdated1",  "admin1"));
//		userDao.save(new User(99,"notUpdated2",  "admin2"));

		System.out.println("CREATE 2 USERS");
		userDao.create2users();
		userDao.getAll().forEach(System.out::println);

	}
}
