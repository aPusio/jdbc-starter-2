package org.example.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class User {
	private Integer userId;
	private String username;
	private String password;

	public User(String username, String password){
		this.username = username;
		this.password = password;
	}

}
