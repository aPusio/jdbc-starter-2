package org.example.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.example.models.User;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UserDao {
	//CREATE - READ - UPDATE - DELETE - CRUD
	//DATA ACCESS OBJECT - DAO

	private Connection connection;

	public void createTableIfNotExists() {
		try {
			connection.createStatement().executeUpdate(
				"CREATE TABLE IF NOT EXISTS Users ( user_id IDENTITY PRIMARY KEY, username CHARACTER(30), password CHARACTER(30));");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void save(User user) {
		//createStatement -> preparedStatement
		try {
			final PreparedStatement preparedStatement = connection.prepareStatement(
				"INSERT INTO Users(username, password) VALUES (?, ?);");
			preparedStatement.setString(1, user.getUsername());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public List<User> getAll() {
		final List<User> users = new ArrayList<>();

		ResultSet resultSet = null;
		try {
			resultSet = connection.createStatement().executeQuery(
				"SELECT * FROM Users");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			while (resultSet.next()) {
				final int userId = resultSet.getInt("user_id");
				final String username = resultSet.getString("username");
				final String password = resultSet.getString("password");
				final User user = new User(userId, username, password);
				users.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}

	public Optional<User> get(int id) {
		ResultSet resultSet2 = null;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from Users where user_id = ? ");
			preparedStatement.setInt(1, id);
			resultSet2 = preparedStatement.executeQuery();

		} catch (Exception e) {
			e.printStackTrace();
		}

		User user = null;
		try {
			if (resultSet2.next()) {
				final int userId = resultSet2.getInt("user_id");
				final String username = resultSet2.getString("username");
				final String password = resultSet2.getString("password");
				user = new User(userId, username, password);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return Optional.ofNullable(user);
	}

	public void remove(int id) {
		if (get(id).isEmpty()) {
			return;
		}
		final PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(
				"DELETE FROM Users WHERE user_id = ?");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void update(User user) {
		if (get(user.getUserId()).isEmpty()) {
			return;
		}

		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(
				"UPDATE Users SET username = ?, password = ? WHERE user_id = ?");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(preparedStatement != null) {
			try {
				preparedStatement.setString(1, user.getUsername());
				preparedStatement.setString(2, user.getPassword());
				preparedStatement.setInt(3, user.getUserId());
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void create2users(){
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		//zapisac tych 2 uzytkownikow
		final User user1 = new User("created1", "hmm");
		final User user2 = new User("created2", "hmm2");
		try {
			final PreparedStatement preparedStatement = connection.prepareStatement(
				"INSERT INTO Users(username, password) VALUES (?, ?);");
			preparedStatement.setString(1, user1.getUsername());
			preparedStatement.setString(2, user1.getPassword());
			preparedStatement.executeUpdate();

			PreparedStatement preparedStatement2 = connection.prepareStatement(
				"INSERT INTO Users(username, password) VALUES (?, ?);");

			preparedStatement2.setString(1, user2.getUsername());
			preparedStatement2.setString(2, user2.getPassword());
			preparedStatement2.executeUpdate();

			connection.commit();
		} catch (SQLException e) {
			try {
				System.out.println("DOING ROLLBACK");
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}

		try {
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

//if(user == USER.EMPTY)

//class Test{
//	public void sampleTest(){
//		//ustawienie falszywego polaczenia
//
//		final UserDao userDao = new UserDao(fakeConnectionMenager);
//
//		//when
//		final List<User> all = userDao.getAll();
//
//		//then
//	}
//}