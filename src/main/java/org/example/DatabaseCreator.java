package org.example;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.example.models.User;

public class DatabaseCreator {
	public static void main(String[] args) throws SQLException {

		//co chcesz zrobic ?
		// dodac / usunac / zmodyfikowac
		// podaaj imie
		//podaj nazwisko
		//brawo stworzyles usera: ...

		//java CLI


		final ConnectionManager connectionManager = new ConnectionManager();
		final Connection connection = connectionManager.getConnection();

		final int updated = connection.createStatement().executeUpdate(
			"CREATE TABLE IF NOT EXISTS Users ( user_id IDENTITY PRIMARY KEY, username CHARACTER(30), password CHARACTER(30));");
		System.out.println("UPDATED: " + updated);

		final int updatedRows = connection.createStatement().executeUpdate(
			"INSERT INTO Users(username, password) VALUES ('Adam', 'haslo');" +
				"INSERT INTO Users(username, password) VALUES ('NieAdam', 'admin1');");
		System.out.println("inserted: " + updatedRows);

		final ResultSet resultSet = connection.createStatement().executeQuery(
			"SELECT * FROM Users");

		while (resultSet.next()){
			final int userId = resultSet.getInt("user_id");
			final String username = resultSet.getString("username");
			final String password = resultSet.getString("password");
//			System.out.println("USER: " + user_id + " " + username + " " + password);
			final User user = new User(userId, username, password);
			System.out.println(user);
		}
		connection.close();
	}
}
