package org.example;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException {
        final ConnectionManager connectionManager = new ConnectionManager();
        final Connection connection = connectionManager.getConnection();

        String userid = "1";

        //TODO przyklad prepared statement
        final PreparedStatement preparedStatement = connection
            .prepareStatement("SELECT * FROM USER WHERE userid = ?");
        preparedStatement.setString(1, userid);

        final int i = preparedStatement.executeUpdate();
        //To samo tylko krocej
        connection.prepareStatement("SELECT ... ").executeUpdate();

        //TODO statement
        connection.createStatement()
            .executeUpdate("SELECT * FROM USER WHERE userid = " + userid);

        System.out.println("zmiany wprowadzone dla: " + i + " rekordow");
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("PAAAANIC !!!");
            e.printStackTrace();
        }
    }
}
